package textgame.maputils;

import java.io.Serializable;

public class Coordinate implements Serializable {
    
    private final int X;
    private final int Y;

    public Coordinate(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    public int getX() {
        return X;
    }
    public int getY() {
        return Y;
    }
}
