package textgame.maputils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ParseJSON {

    public static ParsedMap parseMap(String mapName) {

        int[][] intMap;
        int startX;
        int startY;
        Map<Integer, Set<String>> lockedDirections = null;

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        try {
            String ext = ".json";
            FileReader reader = new FileReader("src/main/java/map/"+mapName+ext);
            //Read JSON file
            JSONObject json = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonMap = (JSONArray) json.get("map");

            int tmp = ((JSONArray) jsonMap.get(0)).size();
            intMap = new int[jsonMap.size()][tmp];

            // Read 2d map
            for(int r = 0 ; r < jsonMap.size(); r++){
                JSONArray inner = (JSONArray) jsonMap.get(r);
                for(int j = 0; j < inner.size(); j++){
                    intMap[r][j] = ((Long) inner.get(j)).intValue();
                }
            }
            startX = ((Long) json.get("start_x")).intValue();
            startY = ((Long) json.get("start_y")).intValue();

            // Get locked doors, if exist
            if(json.containsKey("locked")) {
                lockedDirections = new HashMap<>();
                JSONObject locked = (JSONObject) json.get("locked");
                for (Object r : locked.keySet()) {
                    String row = (String) r;
                    JSONObject cols = (JSONObject) locked.get(row);
                    for (Object c : cols.keySet()) {
                        String col = (String) c;
                        JSONArray directions = (JSONArray) cols.get(col);
                        Set<String> set = new HashSet<>();
                        for (Object direction : directions) {
                            set.add((String) direction);
                        }
                        // store key as flatten 2d coordinate.
                        lockedDirections.put((Integer.parseInt(row)*jsonMap.size()) + Integer.parseInt(col), set);
                    }
                }
            }

            return new ParsedMap(intMap, lockedDirections, startX, startY);
        } catch (IOException | ParseException e) {
            System.out.println("Invalid map file. Please use a valid json file with proper attribute");
        }
        return null;
    }

}