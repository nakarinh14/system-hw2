package textgame.maputils;

import java.util.Map;
import java.util.Set;

public class ParsedMap {

    private int[][] roomMap;
    private Map<Integer, Set<String>> lockedDirection;
    private int startX;
    private int startY;

    public ParsedMap(int[][] roomMap, Map<Integer, Set<String>>  lockedDirection, int startX, int startY) {
        this.roomMap = roomMap;
        this.lockedDirection = lockedDirection;
        this.startX = startX;
        this.startY = startY;
    }

    public int[][] getRoomMap() {
        return roomMap;
    }

    public Map<Integer, Set<String>> getLockedDirection() {
        return lockedDirection;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }
}
