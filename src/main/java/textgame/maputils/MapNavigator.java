package textgame.maputils;

import textgame.roomutils.Room;
import textgame.characters.Monster;
import textgame.items.Item;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapNavigator implements Serializable {
    private Room[][] roomMap;
    private Room currentRoom;
    private Coordinate currentPos;
    Map<Integer, Set<String>> lockedDirections;

    private final Map<String, Coordinate> SET_MOVES = new HashMap<String, Coordinate>() {{
        put("up", new Coordinate(0,-1));
        put("down",  new Coordinate(0,1));
        put("left",  new Coordinate(-1,0));
        put("right", new Coordinate(1,0));
    }};

    // Load the map based on map file name
    public boolean loadMap(String mapName){
        GameMap gameMap = MapConstructor.constructMap(mapName);
        if(gameMap != null) {
            roomMap = gameMap.getRoomMap();
            currentPos = gameMap.getStartingPos();
            lockedDirections = gameMap.getLockedDirections();
            enterRoom(currentPos);
            return true;
        }
        return false;
    }

    // Enter at the coordinate
    private void enterRoom(Coordinate newCoords){
        currentRoom = roomMap[newCoords.getY()][newCoords.getX()];
    }

    public int getMapHeight(){
        return roomMap.length;
    }
    public int getMapWidth(){
        return roomMap[0].length;
    }

    public Coordinate getCurrentPos(){
        return currentPos;
    }

    public boolean isValidCoord(Coordinate coordinate){
        int x = coordinate.getX();
        int y = coordinate.getY();
        return y >= 0 && y < getMapHeight() && x >= 0 && x < getMapWidth();
    }

    public Room getRoom(Coordinate coordinate){
        if(isValidCoord(coordinate)){
            return roomMap[coordinate.getY()][coordinate.getX()];
        }
        return null;
    }

    public boolean move(String direction){
        // Move by the given direction.
        // Return a boolean whether the direction desired is reachable, then change position.
        Coordinate moveValues = SET_MOVES.get(direction);
        if(moveValues != null) {
            Coordinate moved = new Coordinate(
                    currentPos.getX()+moveValues.getX(),
                    currentPos.getY()+moveValues.getY()
            );

            if (isValidCoord(moved)) {
                if (roomMap[moved.getY()][moved.getX()] != null && !isLocked(currentPos, direction)) {
                    System.out.println("You enter the new room.");
                    currentPos = moved;
                    enterRoom(currentPos);
                    return true;
                } else{
                    System.out.println("There room is not accessible in that direction");
                }
            } else {
                System.out.println("You can't move there, it's the border of the map");
            }
        } else {
            System.out.println("Invalid move. You can only go left, right, up, down");
        }
        return false;
    }


    private boolean isLocked(Coordinate coords, String direction){
        if(lockedDirections != null) {
            // flatten coordinate
            int flattenCoord = (coords.getY() * getMapHeight()) + coords.getX();
            if(lockedDirections.containsKey(flattenCoord)) {
                return lockedDirections.get(flattenCoord).contains(direction);
            }
        }
        return false;
    }

    public Item takeItem(){
        return currentRoom.popItem();
    }

    public Item getCurrentItem(){
        return currentRoom.getItem();
    }
    
    public Monster getCurrentEnemy(){
        return currentRoom.getEnemy();
    }

}
