package textgame.maputils;

import textgame.roomutils.Room;
import textgame.roomutils.RoomFactory;


import java.util.Map;
import java.util.Set;

public class MapConstructor {
    // Construct the map of game room from integer mapping
    // Convert 2d dimension array into flatten array.

    public static GameMap constructMap(String mapName){
        ParsedMap parsedMap = ParseJSON.parseMap(mapName);
        if(parsedMap != null){
            Room[][] roomMap;
            Coordinate startingPos = new Coordinate(parsedMap.getStartX(), parsedMap.getStartY());
            int[][] intMap = parsedMap.getRoomMap();
            Map<Integer, Set<String>> lockedDirections = parsedMap.getLockedDirection();

            int height = intMap.length;
            int width = intMap[0].length;

            roomMap = new Room[height][width];

            for (int row = 0; row < height; row++) {
                for (int col = 0; col < width; col++) {
                    int tmp = intMap[row][col];
                    Room room = tmp >= 0 ? RoomFactory.createRoom(tmp) : null;
                    roomMap[row][col] = room;
                }
            }
            return new GameMap(roomMap, startingPos, lockedDirections);
        }
        return null;
    }
}
