package textgame.maputils;

public class MapArt {

    private static void printBorder(MapNavigator navigator, StringBuilder bd){
        for(int i = 0; i < navigator.getMapWidth() * 3; i++){
            bd.append((i == 0 || i == (navigator.getMapWidth() * 3)-1) ? "+" : "-");
        }
        bd.append("\n");
    }

    public static void printMap(MapNavigator navigator) {
        Coordinate currentPos = navigator.getCurrentPos();
        StringBuilder sb = new StringBuilder("\n");

        printBorder(navigator, sb);
        for(int r = 0; r < navigator.getMapHeight(); r++){
            for(int c = 0; c < navigator.getMapWidth(); c++){
                String tmp = "";
                if(currentPos.getY() == r && currentPos.getX() == c) {
                    tmp = " 0 ";
                } else {
                    tmp = navigator.getRoom(new Coordinate(c, r)) != null ? " # ": " . ";
                }
                sb.append(tmp);
            }
            sb.append("\n");
        }
        printBorder(navigator, sb);
        sb.append("\"0\" : Your current position,   \"#\" : Room,   \".\" : Unreachable area.");
        System.out.println(sb.toString());
    }
}
