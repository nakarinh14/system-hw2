package textgame.maputils;

import textgame.roomutils.Room;


import java.util.Map;
import java.util.Set;

public class GameMap {

    private Room[][] roomMap;
    private Coordinate startingPos;
    private Map<Integer, Set<String>> lockedDirections;

    public GameMap(Room[][] roomMap, Coordinate startingPos, Map<Integer, Set<String>> lockedDirections) {
        this.roomMap = roomMap;
        this.startingPos = startingPos;
        this.lockedDirections = lockedDirections;
    }

    public Room[][] getRoomMap() {
        return roomMap;
    }

    public Coordinate getStartingPos() {
        return startingPos;
    }

    public Map<Integer, Set<String>> getLockedDirections() {
        return lockedDirections;
    }
}
