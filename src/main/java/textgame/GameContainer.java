package textgame;

public class GameContainer {

    private Game game;

    public GameContainer(){
        game = new Game();
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }
}
