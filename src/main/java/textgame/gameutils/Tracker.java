package textgame.gameutils;

import textgame.maputils.Coordinate;
import textgame.maputils.MapNavigator;
import textgame.roomutils.Room;

public class Tracker {

    public static boolean isMapCleared(MapNavigator navigator){
        for(int i = 0; i < navigator.getMapHeight(); i++){
            for(int j = 0; j < navigator.getMapWidth(); j++) {
                Room tmp = navigator.getRoom(new Coordinate(j,i));
                if(tmp != null){
                    if(tmp.isEnemyExist()){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
