package textgame.gameutils;

import textgame.characters.Monster;
import textgame.characters.Player;
import textgame.items.Item;

import java.util.List;

public class Info {

    public static void getRoomInfo(Monster enemy, Item item) {
        if (enemy == null) {
            System.out.println("There is no enemy in this room.");
        } else {
            if (enemy.isAlive()) {
                System.out.println("There is an enemy " + enemy.getName() + " in this room.");
                System.out.println(enemy.getName() + "'s HP: " + enemy.getCurrentHP() + "/" + enemy.getMaxHP() + " | " + " AP: " + enemy.getBaseDmg());
            } else {
                System.out.println("You saw the " + enemy.getName() + " corpse on the ground.");
            }
        }

        if (item == null) {
            System.out.println("There is no item in this room");
        } else {
            System.out.println("A " + item.getName() + " is in this room.");
        }
    }

    public static void getPlayerInfo(Player player){
        System.out.println("HP: " + player.getCurrentHP() + " / " + player.getMaxHP());
        System.out.println("AP: " + player.getBaseDmg());
        List<Item> inventories = player.getInventoryList();
        if(inventories.size() == 0){
            System.out.println("There is no item in player inventory");
        } else {
            System.out.print("Items in player inventory: ");
            for(Item item : inventories){
                System.out.print(item.getName() + " . ");
            }
            System.out.println("\n");
        }
    }


}
