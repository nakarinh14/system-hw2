package textgame.gameutils;

import textgame.maputils.Coordinate;
import textgame.maputils.MapNavigator;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class DFSSearch {
    Stack<String> stack;
    Set<Integer> visited;
    MapNavigator navigator;


    public DFSSearch(MapNavigator navigator) {
        this.navigator = navigator;
        stack = new Stack<>();
        visited = new HashSet<>();
        addNode(navigator.getCurrentPos());
        stack.push(null);  // Return place holder.
    }

    public String searchPath(){

        // Use DFS for path searching.

        String[] direction = {"up","right","left","down"};
        String[] reverseDirection = {"down", "left","right","up"};
        int[][] surrounding = {{-1,0}, {0,1}, {0,-1}, {1,0}};

        if(!stack.isEmpty()){
            boolean tmp = false;
            Coordinate curr = navigator.getCurrentPos();
            for(int i = 0; i < 4; i++){
                if(addNode(new Coordinate(surrounding[i][1]+curr.getX(),surrounding[i][0]+curr.getY()))){
                    stack.push(reverseDirection[i]);
                    return direction[i];
                }
            }
            return stack.pop();
        }
        return null;
    }

    private boolean addNode(Coordinate delta){
        int flatten = delta.getX() + (navigator.getMapHeight()*delta.getY());
        if(!visited.contains(flatten) && navigator.getRoom(delta) != null){
            visited.add(flatten);
            return true;
        }
        return false;
    }

}
