package textgame.gameutils;

import textgame.characters.GameCharacter;
import textgame.characters.Player;
import textgame.items.Item;

import java.util.Random;

public class BattleSystem {
    public static void fight(Player player, GameCharacter enemy, Item weapon) {
        // Fight taking turns. Player attack first, and then enemy back.
        // Return boolean if the fight is over.
        if (enemy != null) {
            if (enemy.isAlive()) {

                int playerDmg = player.getBaseDmg() + weapon.getDamage();
                int enemyDmg = enemy.getBaseDmg();

                System.out.println("\nYou deal " + playerDmg + " damage to " + enemy.getName() + ".");
                enemy.setCurrentHP(enemy.getCurrentHP() - playerDmg);
                if (enemy.isAlive()) {
                    System.out.println("The " + enemy.getName() + " have " + enemy.getCurrentHP() + "/" + enemy.getMaxHP() + " HP left. \n");
                    Random r = new Random();
                    enemyDmg += r.nextInt((int) Math.round(enemyDmg*0.2)); // Enemy attack damage is base dmg + random int +/- 20% of base dmg
                    System.out.println(enemy.getName() + " attack you with " + enemyDmg + " damage.");
                    player.setCurrentHP(player.getCurrentHP() - enemyDmg);
                    System.out.println("You have " + player.getCurrentHP() + "/" + player.getMaxHP() + " HP left.");
                } else {
                    System.out.println("You have slained the " + enemy.getName() + ".");
                    // If monster is defeated, increase base damage.
                    player.setBaseDmg(player.getBaseDmg() + 10);
                }
            } else {
                System.out.println("You look at the corpse of the " + enemy.getName() + " that you have already slained.");
            }
        } else{
            System.out.println("The enemy doesn't exist in the room.");
        }
    }
}
