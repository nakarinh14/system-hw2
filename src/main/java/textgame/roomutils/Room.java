package textgame.roomutils;

import textgame.items.Item;
import textgame.characters.Monster;

import java.io.Serializable;

public class Room implements Serializable {
    private Monster enemy;
    private Item item;

    Room(Monster enemy, Item item){
        this.enemy = enemy;
        this.item = item;
    }

    public Monster getEnemy(){
        return enemy;
    }
    public Item getItem(){return item;}

    public Item popItem(){
        Item tmp = item;
        item = null;
        return tmp;
    }

    public boolean isEnemyExist(){
        if(enemy == null) return false;
        if(enemy.isAlive()){
            return true;
        }
        return false;
    }

}
