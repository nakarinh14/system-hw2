package textgame.roomutils;

import textgame.characters.MonsterFactory;
import textgame.items.ItemFactory;

import java.util.HashMap;
import java.util.Map;

public class RoomFactory {

    static Map<Integer, RoomType> roomMapping = new HashMap<Integer, RoomType>() {{
        RoomType[] roomTypes = RoomType.values();
        for(int i = 0; i < roomTypes.length; i++){
            put(i, roomTypes[i]);
        }
    }};

    public static Room createRoom(Integer val){
        RoomType room = roomMapping.get(val);
        if(room != null){
            return new Room(MonsterFactory.createMonster(room.getMonster()), ItemFactory.createItem(room.getItem()));
        }
        return null;
    };
}
