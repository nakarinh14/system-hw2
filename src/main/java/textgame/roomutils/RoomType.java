package textgame.roomutils;
import textgame.characters.MonsterType;
import textgame.items.ItemType;

public enum RoomType {

    EMPTY(null, null),
    TREASURE(MonsterType.ORC, ItemType.IRON_SWORD),
    TRAP(MonsterType.ORC, ItemType.WOOD_SWORD),
    HARD(MonsterType.WITCH, ItemType.GOLD_SWORD),
    BOSS(MonsterType.DRAGON, ItemType.DIAMOND_SWORD);


    private final MonsterType ENEMY;
    private final ItemType ITEM;

    RoomType(MonsterType enemy, ItemType item){
        this.ENEMY = enemy;
        this.ITEM = item;
    }

    public MonsterType getMonster(){
        return ENEMY;
    }

    public ItemType getItem(){
        return ITEM;
    }

}
