package textgame;

import textgame.characters.Player;
import textgame.gameutils.BattleSystem;
import textgame.gameutils.DFSSearch;
import textgame.gameutils.Info;
import textgame.gameutils.Tracker;
import textgame.items.Item;
import textgame.maputils.MapArt;
import textgame.maputils.MapNavigator;

import java.io.Serializable;

public class Game implements Serializable {
    private MapNavigator navigator;
    private Player player;
    private boolean started = false;

    public void loadGame(String mapPath){
        navigator = new MapNavigator();
        if(navigator.loadMap(mapPath)){
            System.out.println("The map loaded successfully");
            started = true;
            player = new Player(100, 15, 15);
        }
    }

    public void battle(String weaponName){
        Item weapon = player.getItem(weaponName);
        if(weapon != null) {
            BattleSystem.fight(player, navigator.getCurrentEnemy(), weapon);
            if (!player.isAlive()) {
                System.out.println("You lose all HP and died from the battle.");
                resetGame();
            } else{
                trackObjective();
            }
        } else{
            System.out.println(weaponName + " is not in the inventory.");
        }
    }

    public void move(String direction){
        System.out.println("You tried to move " + direction + ".");
        boolean moved = navigator.move(direction);
        if(moved){
            player.regenerate();
            printInfo();
        }
    }

    public void takeItem(){
        Item item = navigator.takeItem();
        if(item == null){
            System.out.println("There is no item in this room.");
        } else {
            player.addItem(item);
            System.out.println("You put " + item.getName() + " in your inventory");
        }
    }

    public void dropItem(String item){
        if(player.getItem(item) == null){
            System.out.println(item + " is not in the inventory.");
        } else {
            player.removeItem(item);
            System.out.println(item + " is removed from your inventory");
        }
    }

    public void basicAI(){
        DFSSearch pilot = new DFSSearch(navigator);
        String direction = "";
        while(!trackObjective() && direction != null){
            direction = pilot.searchPath();
            if(direction != null){
                move(direction);
                printMap();
                if(navigator.getCurrentItem() != null){
                    takeItem();
                }
                if(navigator.getCurrentEnemy() != null) {
                    while (navigator.getCurrentEnemy().isAlive()) {
                        battle(player.getBestItem());
                    }
                }
            }
        }
        System.out.println("AI completed. All room visited");
    }

    private boolean trackObjective(){
        boolean cond = Tracker.isMapCleared(navigator);
        if(cond){
            System.out.println("All monsters are defeated. You have won the game!!!");
            resetGame();
        }
        return cond;
    }

    public void resetGame(){
        System.out.println("\n====== Game Over ======\n");
        player = null;
        navigator = null;
        started = false;
    }

    public void printInfo(){
        Info.getRoomInfo(navigator.getCurrentEnemy(), navigator.getCurrentItem());
        Info.getPlayerInfo(player);
    }

    public boolean isPlaying(){
        return started;
    }

    public void printMap(){
        MapArt.printMap(navigator);
    }


}
