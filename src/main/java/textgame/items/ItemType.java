package textgame.items;

public enum ItemType {

    //  Enum for sword.
    WOOD_SWORD(10, "Wooden Sword"),
    IRON_SWORD(20, "Iron Sword"),
    GOLD_SWORD(30, "Golden Sword"),
    DIAMOND_SWORD(40, "Diamond Sword");

    private final int DMG;
    private final String NAME;

    ItemType(int dmg, String name){
        this.DMG = dmg;
        this.NAME = name;
    }
    public int getDmg(){
        return this.DMG;
    }

    public String getName(){
        return this.NAME;
    }

}
