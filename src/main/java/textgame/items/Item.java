package textgame.items;

import java.io.Serializable;

public class Item implements Serializable {

    private final int INCREASE_DMG;
    private final String NAME;

    public Item(int dmg, String name) {
        this.NAME = name;
        INCREASE_DMG = dmg;
    }
    public String getName(){
        return NAME;
    }
    public int getDamage(){return INCREASE_DMG;}
}

