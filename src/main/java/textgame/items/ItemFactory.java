package textgame.items;

public class ItemFactory {

    public static Item createItem(ItemType itemType) {
        return itemType != null ? new Item(itemType.getDmg(), itemType.getName()) : null;
    }
}
