package textgame.characters;

public class MonsterFactory {

    public static Monster createMonster(MonsterType monsterType){
        return monsterType != null ? new Monster(monsterType.getMaxHP(), monsterType.getBaseDmg(), monsterType.getName()) : null;
    }
}
