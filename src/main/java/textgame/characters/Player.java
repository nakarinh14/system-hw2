package textgame.characters;

import textgame.items.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Player extends GameCharacter {

    private final Map<String, Item> INVENTORIES =  new HashMap<>();;
    private final int REGEN_VALUE;

    public Player(int maxHP, int baseDmg, int regenValue){
        super(maxHP, baseDmg, null);
        this.REGEN_VALUE = regenValue;
    }

    public void addItem(Item item){
        if(item != null) {
            INVENTORIES.putIfAbsent(item.getName().toLowerCase(), item);
        }
    }
    public void removeItem(String itemName){
        INVENTORIES.remove(itemName);
    }

    public List<Item> getInventoryList(){
        return new ArrayList<>(INVENTORIES.values());
    }

    public String getBestItem(){
        int max = 0;
        String tmp = "";
        for(Item item: getInventoryList()){
            if(item.getDamage() > max){
                max = item.getDamage();
                tmp = item.getName();
            }
        }
        return tmp;
    }

    public Item getItem(String item){
        return INVENTORIES.get(item.toLowerCase());
    }

    public void regenerate(){
        int increasedHp = getCurrentHP() + REGEN_VALUE;
        if(increasedHp < getMaxHP()){
            setCurrentHP(increasedHp);
        } else {
            setCurrentHP(getMaxHP());
        }
    }
}
