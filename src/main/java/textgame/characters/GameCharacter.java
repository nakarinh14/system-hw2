package textgame.characters;

import java.io.Serializable;

public abstract class GameCharacter implements Serializable {
    private final int MAX_HP;
    private final String NAME;
    private int currentHP;
    private int baseDmg;


    GameCharacter(int maxHP, int baseDmg, String name){
        this.MAX_HP = maxHP;
        this.currentHP = maxHP;
        this.baseDmg = baseDmg;
        this.NAME = name;
    }

    public void setCurrentHP(int hp){
        currentHP = hp;
    }
    public int getCurrentHP(){
        return currentHP;
    }

    public int getMaxHP(){
        return MAX_HP;
    }


    public int getBaseDmg(){
        return baseDmg;
    }
    public void setBaseDmg(int dmg){
        baseDmg = dmg;
    }

    public boolean isAlive(){
        return currentHP > 0;
    }

    public String getName(){return NAME;}
}
