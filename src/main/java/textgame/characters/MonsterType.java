package textgame.characters;

public enum MonsterType {
    ORC(60, 10, "Orc"),
    SKELETON(10, 3, "Skeleton"),
    WITCH(30, 30, "Witch"),
    DRAGON(100, 50, "Dragon");


    private final int MAX_HP;
    private final int BASE_DMG;
    private final String NAME;

    MonsterType(int maxHP, int baseDmg, String name){
        this.MAX_HP = maxHP;
        this.BASE_DMG = baseDmg;
        this.NAME = name;
    }

    public int getMaxHP() {
        return MAX_HP;
    }

    public int getBaseDmg() {
        return BASE_DMG;
    }

    public String getName() {
        return NAME;
    }
}
