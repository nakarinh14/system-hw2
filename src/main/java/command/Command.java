package command;

import textgame.GameContainer;

public interface Command {

    void execute(String[] args, GameContainer game);

    boolean ingameCommand();

}
