package command;

import textgame.Game;
import textgame.GameContainer;

public class InfoCommand implements Command{
    public void execute(String[] args, GameContainer gameContainer) {
        if(gameContainer.getGame().isPlaying()) {
            gameContainer.getGame().printInfo();
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
