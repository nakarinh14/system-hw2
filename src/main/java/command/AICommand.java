package command;

import textgame.GameContainer;

public class AICommand implements Command{

    public void execute(String[] args, GameContainer gameContainer) {

        if(args.length == 1){
            if(gameContainer.getGame().isPlaying()){
                gameContainer.getGame().basicAI();
            }
        }
        else {
            System.out.println("Invalid argument");
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
