package command;

import textgame.GameContainer;
import save.SerializeGame;

public class SaveCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(gameContainer.getGame().isPlaying() && args.length == 2){
            SerializeGame.save(gameContainer.getGame(), args[1]);
        }
    }


    public boolean ingameCommand() {
        return true;
    }
}
