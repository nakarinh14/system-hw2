package command;

import textgame.Game;
import textgame.GameContainer;

public class PlayCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length == 2){
            System.out.println("Loading map for " + "\""+ args[1] + "\"" + "...");
            gameContainer.getGame().loadGame(args[1]);
        } else {
            System.out.println("Invalid arguments. Please use only 1 argument for map name");
        }
    }


    public boolean ingameCommand() {
        return false;
    }
}
