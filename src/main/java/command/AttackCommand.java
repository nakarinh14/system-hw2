package command;

import textgame.GameContainer;

import java.util.Arrays;

public class AttackCommand implements Command {


    public void execute(String[] args, GameContainer gameContainer) {

        if(args.length > 2){
            if(args[1].equals("with")){
                if(gameContainer.getGame().isPlaying()){
                    String tmp = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                    gameContainer.getGame().battle(tmp);
                }
            }
        } else {
            System.out.println("Invalid argument");
        };
    }

    public boolean ingameCommand() {
        return true;
    }
}
