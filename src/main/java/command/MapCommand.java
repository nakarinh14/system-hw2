package command;

import textgame.GameContainer;

public class MapCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(gameContainer.getGame().isPlaying() && args.length == 1){
            gameContainer.getGame().printMap();
        } else{
            System.out.println("Invalid command");
        }
    }


    public boolean ingameCommand() {
        return false;
    }
}
