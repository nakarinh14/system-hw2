package command;

import textgame.GameContainer;

import java.util.Arrays;

public class DropCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length > 1){
            gameContainer.getGame().dropItem(String.join(" ", Arrays.copyOfRange(args, 1, args.length)));
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
