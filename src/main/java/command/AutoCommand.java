package command;

import textgame.GameContainer;

public class AutoCommand implements Command {

    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length == 2 && gameContainer.getGame().isPlaying()) {
            AutoPilot.run(args[1], gameContainer);
        } else {
            System.out.println("Please used valid autopilot file.");
        }
    }

    public boolean ingameCommand() {
        return false;
    }
}
