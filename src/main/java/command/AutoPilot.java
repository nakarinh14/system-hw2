package command;

import textgame.GameContainer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AutoPilot {

    private final static String  FILE_DIR = "src/main/resources/";
    public static void run(String fileInput, GameContainer gameContainer) {
        String fileName = FILE_DIR + fileInput;
        System.out.println(fileName);
        FileInputStream fileInputS = null;
        try {
            fileInputS = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner sc = new Scanner(fileInputS);
        while(sc.hasNextLine()) {
            String[] args = sc.nextLine().split(" ");
            Command tmp = CommandFactory.get(args[0]);
            if(tmp != null) {
                if (tmp.ingameCommand()) {
                    tmp.execute(args, gameContainer);
                }
            }
        }
        sc.close();
    }
}

