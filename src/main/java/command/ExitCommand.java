package command;

import textgame.Game;
import textgame.GameContainer;

public class ExitCommand implements Command{

    public void execute(String[] args, GameContainer gameContainer) {
        if(!gameContainer.getGame().isPlaying()) {
            gameContainer.getGame().resetGame();
            System.exit(0);
        }
    }


    public boolean ingameCommand() {
        return false;
    }


}
