package command;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private static Map<String, Command> commandMap = new HashMap<>();

    static{
        commandMap.put("exit", new ExitCommand());
        commandMap.put("info", new InfoCommand());
        commandMap.put("take", new TakeCommand());
        commandMap.put("drop", new DropCommand());
        commandMap.put("attack", new AttackCommand());
        commandMap.put("go", new GoCommand());
        commandMap.put("map", new MapCommand());
        commandMap.put("autopilot", new AutoCommand());
        commandMap.put("help", new HelpCommand());
        commandMap.put("play", new PlayCommand());
        commandMap.put("load", new LoadCommand());
        commandMap.put("save", new SaveCommand());
        commandMap.put("ai", new AICommand());
        commandMap.put("quit", new QuitCommand());
    }

    public static Command get(String cmd){
        return commandMap.get(cmd);
    }
}
