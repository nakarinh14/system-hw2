package command;

import textgame.Game;
import textgame.GameContainer;

public class HelpCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        System.out.println("info – print out information of the player and the room that the player is currently in, this command only available while playing game. These are:");
        System.out.println("take – take command is used to pick up the item in the current room, this command only available while playing game.");
        System.out.println("drop – drop item of choice that the player currently carries, this command only available while playing game.");
        System.out.println("attack with – is used to attack a monster in the current room, this command only available while playing game.");
        System.out.println("go {direction} – move player to the room as specified by the direction, e.g. north, east, west, south, this command only available while playing game");
        System.out.println("map – print 2D map using ascii art, this command only available while playing game");
        System.out.println("autopilot {file} – run this game in autopilot mode using the list of command in the file, this command only available while playing game");
        System.out.println("help – print all commands");
        System.out.println("quit – end the current game and return to command prompt to let user choose the map or load from saved point again.");
        System.out.println("play {map-name} – play new game, this command only available at when start the game.");
        System.out.println("load {saved-point-name} – load game state from saved point, this command only available at when start the game.");
        System.out.println("save {saved-point-name} – load game state from saved point, this command only available while playing game.");
        System.out.println("exit – exit whole game, this command only available at when start the game.");
    }

    public boolean ingameCommand() {
        return true;
    }
}
