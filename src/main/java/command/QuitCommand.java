package command;

import textgame.GameContainer;

public class QuitCommand implements Command{

    public void execute(String[] args, GameContainer gameContainer) {
        if(gameContainer.getGame().isPlaying()) {
            gameContainer.getGame().resetGame();
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
