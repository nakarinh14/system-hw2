package command;

import textgame.Game;
import textgame.GameContainer;
import save.SerializeGame;

public class LoadCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length == 2 && !gameContainer.getGame().isPlaying()){
            Game game = SerializeGame.load(args[1]);
            if(game != null){
                gameContainer.setGame(game);
            }
        }
    }

    public boolean ingameCommand() {
        return false;
    }
}
