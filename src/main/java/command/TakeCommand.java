package command;

import textgame.Game;
import textgame.GameContainer;

public class TakeCommand implements Command{
    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length == 1) {
            if(gameContainer.getGame().isPlaying()){
                gameContainer.getGame().takeItem();
            }
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
