package command;

import textgame.GameContainer;

public class GoCommand implements Command {
    public void execute(String[] args, GameContainer gameContainer) {
        if(args.length == 2) {
            if (gameContainer.getGame().isPlaying()) {
                gameContainer.getGame().move(args[1]);
            } else {
                System.out.println("The game is not loaded yet.");
            }
        } else{
            System.out.println("Invalid argument. Please use 1 argument for the direction");
        }
    }

    public boolean ingameCommand() {
        return true;
    }
}
