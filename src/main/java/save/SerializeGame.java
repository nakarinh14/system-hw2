package save;


import textgame.Game;

import java.io.*;


public class SerializeGame {

    private static final String GAME_DIR = "src/main/java/save/";
    public static void save(Game game, String savePath) {
        try {
            FileOutputStream fileOut = new FileOutputStream(GAME_DIR +savePath+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(game);
            out.close();
            fileOut.close();
            System.out.println("Checkpoint save is saved as " + GAME_DIR + ".");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static Game load(String savePath){

        try {
            FileInputStream fileIn = new FileInputStream(GAME_DIR +savePath+".ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Game game = (Game) in.readObject();
            in.close();
            fileIn.close();
            System.out.println("Checkpoint save loaded.");
            return game;
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Checkpoint load failed. Save game might not exist or corrupted");
        }
        return null;
    }

}
