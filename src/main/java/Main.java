import command.Command;
import command.CommandFactory;
import textgame.Game;
import textgame.GameContainer;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        GameContainer gameContainer = new GameContainer();
        String cmd;
        System.out.println("Welcome to the text game!");
        while(true){
            cmd = scanner.nextLine();
            String[] words = cmd.split(" ");
            Command command = CommandFactory.get(words[0]);
            if(command != null) {
                command.execute(words, gameContainer);
            }
            else {
                System.out.println("Invalid command. Please try again.");
            }
        }
    }
}
